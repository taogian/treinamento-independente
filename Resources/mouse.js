var win = Ti.UI.createWindow({
	backgroundColor: 'yellow'
});

var img01 = Ti.UI.createImageView({
	image: '/assets/images/Itchy.jpg',
	width: '50%',
	height: '50%',
	top: 0,
	left: 0
});

var img02 = Ti.UI.createImageView({
	image: '/assets/images/Jerry.png',
	width: '50%',
	height: '50%',
	top: 0,
	left: '50%'
});

var img03 = Ti.UI.createImageView({
	image: '/assets/images/ligeirinho.jpg',
	width: '50%',
	height: '50%',
	top: '50%',
	left: 0
});

var img04 = Ti.UI.createImageView({
	image: '/assets/images/pinky.jpg',
	width: '50%',
	height: '50%',
	top: '50%',
	left: '50%'
});


win.add(img01);
win.add(img02);
win.add(img03);
win.add(img04);

exports.win = win;
//exports.view = view_car_1;
