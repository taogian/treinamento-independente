var win = Ti.UI.createWindow();

var view = Ti.UI.createView();

var iphoneLabel = Ti.UI.createLabel({
	text: 'This is an iphone',
	height: 50,
	width: 'auto',
	top: 200
	
});


var androidLabel = Ti.UI.createLabel({
	text: 'This is an android',
	height: 50,
	width: 'auto',
	top: 200
});

var PLATFORM = Ti.Platform.osname;

if(PLATFORM === 'iphone')
{
	view.add(iphoneLabel);
} else if(PLATFORM === 'android'){
	view.add(androidLabel);
}

win.add(view);

exports.win = win;
