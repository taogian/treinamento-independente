var win = Ti.UI.createWindow({
	backgroundColor: 'yellow'
});

var switch01 = Ti.UI.createSwitch({
	top: 100,
	value: true,
});

var label01 = Ti.UI.createLabel({
	text: 'Press the switch above',
	height: 40,
	width: 'auto',
	top: 160
});

switch01.addEventListener('change',function(e){
	label01.text = e.value,
});

win.add(switch01);
win.add(label01);

exports.win = win;

