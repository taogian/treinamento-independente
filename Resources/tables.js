var c1 = require('Tables/mouse');
var c2 = require('Tables/slider');
var c3 = require('Tables/ostest');
var c4 = require('Tables/switch');

var view = Ti.UI.createView();

var data = [

	{title: 'No place for Mickey Mouse!', hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
	{title: "Gliding n' sliding", hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
	{title: "Ios or Android!?", hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
	{title: "Switchs", hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
];

var table = Ti.UI.createTableView({
	data: data,
	width: Ti.UI.MAX,
});

//window.add(table);
view.add(table);

table.addEventListener('click',function(e){
	if(e.index === 0)
	{
		//alert(e.index);
		c1.win.open();
	}
	else if(e.index === 1)
	{
		alert(e.index);
		//c2.swin.open();
	}
	else if(e.index === 2)
	{
		c3.win.open();
	}
	else if(e.index === 3)
	{
		c4.win.open();
	}
});

view.add(table);

//exports this hole stuff on a view
exports.view = view;
